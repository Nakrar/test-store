#### Run devserver:

1. install requirements
    ```
    pip install -r requirements.txt
    ```
2. apply DB migrations
    ```
    python manage.py migrate
    ```
3. run server
    ```
    python manage.py runserver
    ```
     
* you can access access admin panel using login/pass: **admin**

#### Run tests:    
1. install requirements
    ```
    pip install -r requirements.txt
    ```
2. run tests
    ```
    python manage.py test
    ```

#### Diversey from the specification

* For ID was used UUID instead of incremental INT. Justified by security reasons.
* For creation timestamp used different field name. Justified by code consistency and readability.
