from collections import defaultdict

from django.core.exceptions import ValidationError
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response

from store.models import Purchaser, Product, PurchaserProduct
from store.serializers import PurchaserSerializer, ProductSerializer, PurchaserProductAPISerializer


class PurchaserCreateView(generics.CreateAPIView):
    serializer_class = PurchaserSerializer
    queryset = Purchaser.objects.all()


class ProductCreateView(generics.CreateAPIView):
    serializer_class = ProductSerializer
    queryset = Product.objects.all()


class PurchaserProductCreateView(generics.CreateAPIView):
    serializer_class = PurchaserProductAPISerializer
    queryset = PurchaserProduct.objects.all()


@api_view(['GET'])
def purchaser_product(request, pk):
    qs = Purchaser.objects.all()
    purchaser = get_object_or_404(qs, pk=pk)

    purchaserproduct_set = purchaser.purchaserproduct_set.all()

    start_date = request.GET.get('start_date')
    end_date = request.GET.get('end_date')

    if start_date:
        try:
            purchaserproduct_set = purchaserproduct_set.filter(timestamp__date__lte=start_date)
        except ValidationError:
            return Response(f'Unable to parse start_date - "{start_date}" to date', status=400)

    if end_date:
        try:
            purchaserproduct_set = purchaserproduct_set.filter(timestamp__date__gte=end_date)
        except ValidationError:
            return Response(f'Unable to parse start_date - "{end_date}" to date', status=400)

    values = purchaserproduct_set.order_by(
        '-timestamp'
    ).values(
        'timestamp__date', 'product__name'
    )

    purchases = defaultdict(list)
    for v in values:
        purchases[v['timestamp__date'].strftime("%Y-%m-%d")].append({'product': v['product__name']})

    return Response({"purchases": purchases})
