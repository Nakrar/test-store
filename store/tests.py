import datetime
import uuid

import pytz
from django.utils import timezone
from rest_framework.test import APITestCase

from store.models import PurchaserProduct, Purchaser, Product


class PurchaserApiTest(APITestCase):
    API_URL = '/store/purchaser/'

    def setUp(self):
        PurchaserProduct.objects.all().delete()
        Purchaser.objects.all().delete()
        Product.objects.all().delete()

    def test_can_create_purchaser(self):
        name = 'Bob'

        request = self.client.post(self.API_URL, {'name': name}, format='json')

        self.assertEqual(request.status_code, 201)
        self.assertEqual(Purchaser.objects.count(), 1)
        self.assertEqual(Purchaser.objects.first().name, name)
        self.assertAlmostEqual(Purchaser.objects.first().created_at, timezone.now(),
                               delta=datetime.timedelta(seconds=1))

    def test_cannot_create_duplicate_names(self):
        name = 'Bob'

        request = self.client.post(self.API_URL, {'name': name}, format='json')
        self.assertEqual(request.status_code, 201)

        request = self.client.post(self.API_URL, {'name': name}, format='json')
        self.assertEqual(request.status_code, 400)

        self.assertEqual(Purchaser.objects.count(), 1)
        self.assertEqual(Purchaser.objects.first().name, name)

    def test_can_get_purchased_products(self):
        desired_data = {
            'purchases': {
                '2019-05-10': [
                    {
                        'product': 'Egg'
                    }, {
                        'product': 'Cheese'
                    }
                ],
                '2019-04-01': [
                    {
                        'product': 'Tomato'
                    }
                ],
                '2019-03-21': [
                    {
                        'product': 'Tomato'
                    }, {
                        'product': 'Cheese'
                    }
                ],
            }
        }

        purchaser1 = Purchaser.objects.create(name='Bob')
        purchaser2 = Purchaser.objects.create(name='Tom')

        tomato = Product.objects.create(name='Tomato')
        cheese = Product.objects.create(name='Cheese')
        egg = Product.objects.create(name='Egg')

        # purchaser1
        # validate sort order for 3-21 and 5-10
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=cheese,
            timestamp=timezone.localtime().replace(year=2019, month=3, day=21, hour=11))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato,
            timestamp=timezone.localtime().replace(year=2019, month=3, day=21, hour=12))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=egg,
            timestamp=timezone.localtime().replace(year=2019, month=5, day=10, hour=12))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=cheese,
            timestamp=timezone.localtime().replace(year=2019, month=5, day=10, hour=11))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=4, day=1))

        # purchaser2
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=5, day=20))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=5, day=10))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=4, day=1))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=3, day=21))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=3, day=1))

        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/')
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json(), desired_data)

        # test for invalid PK
        request = self.client.get(f'{self.API_URL}{uuid.uuid4()}/product/')
        self.assertEqual(request.status_code, 404)

        # test for empty PK
        request = self.client.get(f'{self.API_URL}/product/')
        self.assertEqual(request.status_code, 404)

    def test_filter_purchased_products(self):
        purchases_05_10 = {'2019-05-10': [{
            'product': 'Tomato'
        }]}
        purchases_04_01 = {'2019-04-01': [{
            'product': 'Tomato'
        }]}
        purchases_03_21 = {'2019-03-21': [{
            'product': 'Tomato'
        }]}

        purchaser1 = Purchaser.objects.create(name='Bob')
        purchaser2 = Purchaser.objects.create(name='Tom')

        tomato = Product.objects.create(name='Tomato')

        # purchaser1
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=3, day=21))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=5, day=10))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=4, day=1))

        # purchaser2
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=5, day=20))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=5, day=10))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=4, day=1))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=3, day=21))
        PurchaserProduct.objects.create(
            purchaser=purchaser2, product=tomato, timestamp=timezone.localtime().replace(year=2019, month=3, day=1))

        # no filter
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/')
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {**purchases_05_10, **purchases_04_01, **purchases_03_21})

        # filter start_date
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/', data={'start_date': '2019-05-09'})
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {**purchases_04_01, **purchases_03_21})

        # filter end_date
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/', data={'end_date': '2019-03-22'})
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {**purchases_05_10, **purchases_04_01})

        # filter start_date & end_date
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/',
                                  data={'start_date': '2019-05-09', 'end_date': '2019-03-22'})
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {**purchases_04_01})

        # filter start_date & end_date at same day
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/',
                                  data={'start_date': '2019-04-01', 'end_date': '2019-04-01'})
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {**purchases_04_01})

        # filter start_date & end_date with no intersection
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/',
                                  data={'start_date': '2019-03-29', 'end_date': '2019-04-01'})
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], {})

        # test for invalid format
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/',
                                  data={'start_date': '2019-04-32', 'end_date': '19-03-29', 'asd_date': 'asd'})
        self.assertEqual(request.status_code, 400)

    def test_purchased_products_TZ_sort(self):
        purchases_05_10 = {'2019-05-10': [
            {
                'product': 'Egg'
            }, {
                'product': 'Tomato'
            }, {
                'product': 'Cheese'
            }
        ]}

        purchaser1 = Purchaser.objects.create(name='Bob')

        tomato = Product.objects.create(name='Tomato')
        cheese = Product.objects.create(name='Cheese')
        egg = Product.objects.create(name='Egg')

        # purchaser1
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=cheese,
            timestamp=timezone.localtime().replace(year=2019, month=5, day=10, hour=1))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=tomato,
            timestamp=timezone.localtime().replace(year=2019, month=5, day=10, hour=12))
        PurchaserProduct.objects.create(
            purchaser=purchaser1, product=egg,
            timestamp=timezone.localtime().replace(year=2019, month=5, day=10, hour=23))

        # all 3 products are returned as same day product with reversed Timestamp sort
        request = self.client.get(f'{self.API_URL}{purchaser1.id}/product/')
        self.assertEqual(request.status_code, 200)
        self.assertEqual(request.json()['purchases'], purchases_05_10)


class ProductApiTest(APITestCase):
    API_URL = '/store/product/'

    def setUp(self):
        Product.objects.all().delete()

    def test_can_create_product(self):
        name = 'Tomato'

        request = self.client.post(self.API_URL, {'name': name}, format='json')

        self.assertEqual(request.status_code, 201)
        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.first().name, name)
        self.assertAlmostEqual(Product.objects.first().created_at, timezone.localtime(),
                               delta=datetime.timedelta(seconds=1))

    def test_cannot_create_duplicate_names(self):
        name = 'Tomato'

        request = self.client.post(self.API_URL, {'name': name}, format='json')
        self.assertEqual(request.status_code, 201)

        request = self.client.post(self.API_URL, {'name': name}, format='json')
        self.assertEqual(request.status_code, 400)

        self.assertEqual(Product.objects.count(), 1)
        self.assertEqual(Product.objects.first().name, name)


class PurchaserProductApiTest(APITestCase):
    API_URL = '/store/purchaser-product/'

    def setUp(self):
        PurchaserProduct.objects.all().delete()
        Purchaser.objects.all().delete()
        Product.objects.all().delete()

    def test_can_create_product(self):
        purchaser = Purchaser.objects.create(name='Bob')
        product = Product.objects.create(name='Tomato')

        jst_dt = datetime.datetime.utcnow().replace(microsecond=0).replace(tzinfo=pytz.timezone('Asia/Tokyo'))
        utc_dt = jst_dt.astimezone(pytz.timezone('UTC'))
        jst_timestamp = int(datetime.datetime.timestamp(jst_dt))

        data = {
            'purchaser_id': purchaser.id,
            'product_id': product.id,
            'purchase_timestamp': jst_timestamp,
        }

        request = self.client.post(self.API_URL, data, format='json')

        self.assertEqual(request.status_code, 201)
        self.assertEqual(PurchaserProduct.objects.count(), 1)
        self.assertEqual(PurchaserProduct.objects.first().purchaser, purchaser)
        self.assertEqual(PurchaserProduct.objects.first().product, product)
        # check for correct JST -> UTC conversion
        self.assertEqual(PurchaserProduct.objects.first().timestamp, utc_dt)

    def test_cannot_create_duplicate_entries(self):
        purchaser = Purchaser.objects.create(name='Bob')
        product = Product.objects.create(name='Tomato')
        timestamp = int(datetime.datetime.timestamp(datetime.datetime.utcnow()))

        data = {
            'purchaser_id': purchaser.id,
            'product_id': product.id,
            'purchase_timestamp': timestamp,
        }

        request = self.client.post(self.API_URL, data, format='json')
        self.assertEqual(request.status_code, 201)

        request = self.client.post(self.API_URL, data, format='json')
        self.assertEqual(request.status_code, 400)

        self.assertEqual(PurchaserProduct.objects.count(), 1)
