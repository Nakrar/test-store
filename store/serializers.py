import datetime
import time

from django.utils.timezone import make_aware, make_naive
from rest_framework import serializers
from rest_framework.relations import PrimaryKeyRelatedField

from store.models import Product, Purchaser, PurchaserProduct


class UnixEpochDateField(serializers.DateTimeField):
    def to_representation(self, value):
        """ Return epoch time for a datetime object or ``None``"""
        dt = make_naive(value)
        try:
            return int(time.mktime(dt.timetuple()))
        except (AttributeError, TypeError):
            return None

    def to_internal_value(self, value):
        dt = datetime.datetime.fromtimestamp(int(value))
        return make_aware(dt)


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'name',)


class PurchaserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Purchaser
        fields = ('id', 'name',)


# maps API specification fields to actual model fields
class PurchaserProductAPISerializer(serializers.ModelSerializer):
    purchase_timestamp = UnixEpochDateField(source='timestamp')
    purchaser_id = PrimaryKeyRelatedField(source='purchaser', queryset=Purchaser.objects.all())
    product_id = PrimaryKeyRelatedField(source='product', queryset=Product.objects.all())

    class Meta:
        model = PurchaserProduct
        fields = ('id', 'purchase_timestamp', 'purchaser_id', 'product_id',)
