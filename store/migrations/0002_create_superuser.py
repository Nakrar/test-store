from django.db import migrations
from django.contrib.auth.admin import User


def create_superuser(*args, **kwargs):
    User.objects.create_superuser(
        username='admin',
        email='admin@admin.com',
        password='admin',
    )


class Migration(migrations.Migration):
    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(create_superuser)
    ]
