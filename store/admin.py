from django.contrib import admin
from django.contrib.admin import ModelAdmin

from store.models import Product, Purchaser, PurchaserProduct


@admin.register(Product)
class ProductAdmin(ModelAdmin):
    list_display = (
        'name',
        'id',
    )
    search_fields = (
        'id',
        'name',
    )


@admin.register(Purchaser)
class PurchaserAdmin(ModelAdmin):
    list_display = (
        'name',
        'id',
    )
    search_fields = (
        'id',
        'title',
    )


@admin.register(PurchaserProduct)
class PurchaserProductAdmin(ModelAdmin):
    list_display = (
        'id',
        'purchaser',
        'product',
        'created_at',
    )
    search_fields = (
        'id',
        'created_at',
        'phone_number',
    )
