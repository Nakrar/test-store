from django.urls import path

from store import views

urlpatterns = [
    path('purchaser/', views.PurchaserCreateView.as_view()),
    path('purchaser/<uuid:pk>/product/', views.purchaser_product),
    path('product/', views.ProductCreateView.as_view()),
    path('purchaser-product/', views.PurchaserProductCreateView.as_view()),
]
